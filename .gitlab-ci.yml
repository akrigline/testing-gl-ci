# Cache modules in between jobs
# https://docs.gitlab.com/ee/ci/caching/#cache-nodejs-dependencies
cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - .npm/

workflow:
  # Run this workflow when a "release-" tag is pushed or created
  rules:
    - if: $CI_COMMIT_TAG =~ /^release-/

variables:
  ZIP_FILE_NAME: "tester-$CI_COMMIT_TAG.zip"
  RELEASE_DOWNLOAD: "https://gitlab.com/$CI_PROJECT_PATH/-/releases/$CI_COMMIT_TAG/downloads/tester-$CI_COMMIT_TAG.zip"

build:
  image: node:latest
  stage: build
  script:
    # Extract version from module.json
    - PACKAGE_VERSION=$(node -p "require('./module.json').version")
    - PACKAGE_DOWNLOAD=$(node -p "require('./module.json').download")

    # Validate that the tag being released matches the package version.
    - |
      if [[ ! $CI_COMMIT_TAG == release-$PACKAGE_VERSION ]]; then
        echo "The module.json version does not match tag name."
        echo "module.json: $PACKAGE_VERSION"
        echo "tag name: $CI_COMMIT_TAG"
        echo "Please fix this and push the tag again."
        exit 1
      fi

    # Validate that the package download url matches the release asset that will be created.
    - |
      if [[ ! $RELEASE_DOWNLOAD == $PACKAGE_DOWNLOAD ]]; then
        echo "The module.json download url does not match the created release asset url."
        echo "module.json: $PACKAGE_DOWNLOAD"
        echo "release asset url: $RELEASE_DOWNLOAD"
        echo "Please fix this and push the tag again."
        exit 1
      fi

    # # Use cached npm
    # - npm ci --cache .npm --prefer-offline

    # # Run build script
    # - npm run build

    # install the zip package
    - apt-get update
    - apt-get install zip

    # zip whitelist of files to distribute
    - zip $ZIP_FILE_NAME -r module.json LICENSE styles/ scripts/ templates/ languages/
  artifacts:
    # include the manifest json and created zip file as artifacts of the job
    paths:
      - $ZIP_FILE_NAME
      - module.json
    expire_in: never

# 3. Attach build artifacts as release assets
# https://docs.gitlab.com/ee/ci/yaml/index.html#complete-example-for-release
release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo 'running release_job'
  release:
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    description: '**Installation:** To manually install this release, please use the following manifest URL: https://gitlab.com/$CI_PROJECT_PATH/-/releases/$CI_COMMIT_TAG/downloads/module.json'
    assets:
      links:
        # links the release to the manifest from this CI workflow
        - name: 'module.json'
          url: https://gitlab.com/$CI_PROJECT_PATH/-/jobs/artifacts/$CI_COMMIT_TAG/raw/module.json?job=build
          filepath: '/module.json'
        # links the release to the zip file from this CI workflow
        - name: '$ZIP_FILE_NAME'
          url: https://gitlab.com/$CI_PROJECT_PATH/-/jobs/artifacts/$CI_COMMIT_TAG/raw/$ZIP_FILE_NAME?job=build
          filepath: '/$ZIP_FILE_NAME'

updateLatest:
  stage: .post
  script:
    - git tag -f latest
    - git push origin latest -f